import numpy as np
import sys

from cdcm import *


np.core.arrayprint._line_width = 380
np.set_printoptions(precision=3)

def benchmark(mu, nu, communities, T, printfile = False):
  if printfile:
    pf = open(f'figures/benchmark/mu{mu}-nu{nu}-output.txt','w')
  else:
    pf = sys.stdout

  N = np.sum(communities)
  c = communities.shape[0]
  
  print('Generating timeseries...', flush=True, file=pf)
  market_mode = mu * np.tile(
    np.random.normal(size=(1,T)),
    reps = (N, 1)
  )

  noise = nu * np.random.normal(size=(N,T))

  community_mode = np.repeat(
    np.random.normal(size=(c,T)),
    communities,
    axis = 0
  )
  raw_ts = market_mode + noise + community_mode
  
  print('Computing correlation matrix...', flush=True, file=pf)
  ts = TimeSeries(raw_ts)
  shuffled_ts = ts.shuffled(seed=int(1000*mu+10*nu))
  C = ts.compute_correlation()
  print('Diagonalizing correlation matrix...', flush=True, file=pf)
  rmtf = RMTFilter(C)
  rmtf_shuffled = RMTFilter(shuffled_ts.compute_correlation())
  Cm = rmtf.extract_market_mode()
  Cr = rmtf.extract_noise(T=T)
  Cg = rmtf.extract_structure()

  # print(rmtf.eigval)

  print('Plotting correlation matrix...', flush=True, file=pf)
  correlation_plot(C , filename=f'figures/benchmark/mu{mu}-nu{nu}-C.png')
  correlation_plot(Cm, filename=f'figures/benchmark/mu{mu}-nu{nu}-Cm.png')
  correlation_plot(Cr, filename=f'figures/benchmark/mu{mu}-nu{nu}-Cr.png')
  correlation_plot(Cg, filename=f'figures/benchmark/mu{mu}-nu{nu}-Cg.png')
  # correlation_plot(block_average(Cg, communities), filename='figures/benchmark/block-averaged-Cg.png')
  # correlation_plot(block_average(Cm, communities), filename='figures/benchmark/block-averaged-Cm.png')

  eigenvalues_plot(
    (rmtf.eigval, 'Generated Time Series'),
    (rmtf_shuffled.eigval, 'Shuffled Time Series'),
    mp=True,
    N=N,
    T=T,
    bins=40,
    filename=f'figures/benchmark/mu{mu}-nu{nu}-eigenvalues.png',
    xlim=(0,3.*(1. + np.sqrt(N/T))**2),
    ylim=.4/np.sqrt(N/T)
    # yscale='log'
  )

  true_communities = np.repeat(
    np.arange(len(communities)),
    communities
  )

  # Random guess
  print('Random guess: ', shared_information_distance(np.random.randint(0,len(communities), size=(len(true_communities),)), true_communities), file=pf)

  # Asset Graph
  naive_ag = AssestGraph(C)
  naive_ag.estimate_threshold(T)
  naive_ag.fit()
  print('Naive assets graph: ', shared_information_distance(naive_ag.find_communities(), true_communities), file=pf)

  filtered_ag = AssestGraph(Cg)
  filtered_ag.estimate_threshold(T)
  filtered_ag.fit()
  print('Filtered assets graph: ', shared_information_distance(filtered_ag.find_communities(), true_communities), file=pf)

  # MST
  naive_mst = MinimumSpanningTree(C)
  naive_mst.fit()
  print('Naive MST: ', shared_information_distance(naive_mst.find_communities(len(communities)), true_communities), file=pf)

  filtered_mst = MinimumSpanningTree(Cg)
  filtered_mst.fit()
  print('Filtered MST: ', shared_information_distance(filtered_mst.find_communities(len(communities)), true_communities), file=pf)

  # # PMFG
  # naive_pmfg = PMFG(C)
  # naive_pmfg.fit()
  # print('Naive PMFG: ', shared_information_distance(naive_pmfg.find_communities(len(communities)), true_communities))
  # print(shared_information_distance(naive_pmfg.find_communities(len(communities)), naive_mst.find_communities(len(communities))))

  # filtered_pmfg = PMFG(Cg)
  # filtered_pmfg.fit()
  # print('Filtered PMFG: ', shared_information_distance(filtered_pmfg.find_communities(len(communities)), true_communities))

  # pos = graph_plot(N, naive_pmfg.filtered_edge_list, communities=true_communities, filename = 'naive-pmfg.pdf')
  # graph_plot(N, naive_mst.filtered_edge_list, pos = pos, communities=naive_mst.find_communities(len(communities)), filename = 'naive-mst.pdf', labels=range(N))

  # Potts
  filtered_potts = PottsMethod(Cg)
  naive_potts = PottsMethod(C)
  naive_potts.fit(T0 = .00005, steps = 50000, verbose = True, theta=-1000.)
  filtered_potts.fit(T0 = .00005, steps = 50000, verbose = True, theta=-1000.)
  print('Naive Potts Method: ', shared_information_distance(naive_potts.find_communities(), true_communities), file=pf)
  print('Filtered Potts Method: ', shared_information_distance(filtered_potts.find_communities(), true_communities), file=pf)
  energy_plot(naive_potts.steps, naive_potts.energies, filename=f'figures/benchmark/mu{mu}-nu{nu}-naive-potts-energy-plot.png')
  energy_plot(filtered_potts.steps, filtered_potts.energies, filename=f'figures/benchmark/mu{mu}-nu{nu}-filtered-potts-energy-plot.png')
  print('Filtered Potts Comm.: ', filtered_potts.find_communities(), file=pf)

  if pf != sys.stdout:
    pf.close()

if __name__ == '__main__':
  community_sizes = np.array(
    [35, 60, 85, 110, 140, 165, 190, 215],
    dtype=np.int32
  )
  T = 5000

  # community_sizes = np.array(
  #   list(range(50,101,20)),
  #   dtype=np.int32
  # )
  # T = 5000

  # benchmark(mu=.4, nu=.4, communities=community_sizes, T=T, printfile=True)
  # benchmark(mu=1., nu=.4, communities=community_sizes, T=T, printfile=True)
  # benchmark(mu=5., nu=.4, communities=community_sizes, T=T, printfile=True)
  # benchmark(mu=.4, nu=1., communities=community_sizes, T=T, printfile=True)
  # benchmark(mu=1., nu=1., communities=community_sizes, T=T, printfile=True)
  # benchmark(mu=5., nu=1., communities=community_sizes, T=T, printfile=True)
  # benchmark(mu=.4, nu=5., communities=community_sizes, T=T, printfile=True)
  benchmark(mu=1., nu=5., communities=community_sizes, T=T, printfile=True)
  # benchmark(mu=5., nu=5., communities=community_sizes, T=T, printfile=True)