from tabnanny import verbose
from tqdm import tqdm
import numpy as np

class PottsMethod():

  def _hamiltonian(self, part):
    adj_matrix = np.fromfunction(lambda i, j: part[i]==part[j], (self.N, self.N), dtype=int)
    return -1./self.Cnorm * np.einsum('ij,ij->',self.C, adj_matrix)

  def __init__(self, C, n_q = None):
    self.N = int(C.shape[0])
    if n_q is None:
      n_q = min(self.N // 2, 80) # Arbitrary thresholds
    self.n_q = n_q
    self.C = C

    self._partition = np.random.randint(0, n_q, size=(self.N,))
    self._partition_count = [0 for _ in range(n_q)]
    self._elements_in_partition = {p:list() for p in range(n_q)}
    for e, p in enumerate(self._partition):
      self._partition_count[p] += 1
      self._elements_in_partition[p].append(e)
    self._partition_set = list(set(self._partition)) # I use list instead of sets because I prefer O(1) sampling and O(n) remove

    self.Cnorm = np.sum(self.C)
    self._energy = self._hamiltonian(self._partition)

    self.steps = [0]
    self.energies = [self._energy]

    self._best_energy = self._energy
    self._best_partition = self._partition

  def fit(self, T0 = 1., steps = None, verbose = False, theta = 0.99, break_small = True):
    if steps is None:
       steps = self.N * self.n_q

    def temperature(x):
        return T0*(1-(1-theta)**(1-x))/theta

    if break_small:
      def get_candidate():
        while True:
          partition_candidate = np.random.choice(self._partition_set)
          candidate = np.random.choice(self._elements_in_partition[partition_candidate])
          q_candidate =  np.random.choice(self._partition_set)
          if self._partition[candidate] != q_candidate:
            break
        return candidate, q_candidate
    else:
      def get_candidate():
        while True:
          candidate = np.random.randint(0, self.N)
          q_candidate =  np.random.choice(self._partition_set)
          if self._partition[candidate] != q_candidate:
            break
        return candidate, q_candidate

    for t in tqdm(range(steps), disable=(not verbose)):
      if len(self._partition_set) == 1:
        print('Single partition found!')
        break
      
      # Candidate generation
      candidate, q_candidate = get_candidate()
      new_partition = np.copy(self._partition)
      new_partition[candidate] = q_candidate
      new_energy = self._hamiltonian(new_partition)

      # Update of the partition
      if np.random.random() < np.exp(-(new_energy-self._energy)/temperature(t/steps)):
        # Partition count
        old_q = self._partition[candidate]
        self._elements_in_partition[old_q].remove(candidate)
        self._elements_in_partition[q_candidate].append(candidate)
        self._partition_count[old_q] -=1
        if self._partition_count[old_q] == 0:
          self._partition_set.remove(old_q)
        self._partition_count[q_candidate] += 1

        self._partition = new_partition
        self._energy = new_energy

        assert(self._partition_count[q_candidate]>1)
      
      # Variable update
      if self._best_energy > self._energy:
        self._best_energy = self._energy
        self._best_partition = self._partition
      
      self.steps.append(self.steps[-1]+1)
      self.energies.append(self._energy)
      

  def find_communities(self):
    inverse_map = {qp:p for p, qp in enumerate(set(self._best_partition))}
    return np.array([inverse_map[qp] for qp in self._best_partition])

    


