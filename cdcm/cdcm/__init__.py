# Community Dectection for Correlation Matrices
__pkgname__ = 'cdcm'
__author__  = 'Luca Arnaboldi'

from .series import *
from .RMT import *
from .utilities import *
from .graph import *
from .modularity import *
