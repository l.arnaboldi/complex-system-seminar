import enum
from platform import java_ver
import re
import numpy as np
import matplotlib.pyplot as plt
import seaborn

## Plotting
class plot_style():
  def __init__(self, fontsize=12, latex = True, style='seaborn'):
    # Some check on MatplotLib import
    self.fontsize = fontsize
    self.latex = latex
    self.style = style
  def __enter__(self):
    if self.latex:
      plt.rc('text', usetex=True)
      plt.rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{siunitx} \usepackage{amsfonts}')
    plt.rc('font', size=self.fontsize)
    plt.style.use(self.style)
  
  def __exit__(self, *args):
    plt.close('all')
    plt.rcdefaults()
    plt.style.use('default')

def correlation_plot(correlation_matrix, show = False, filename = None,):
  with plot_style():
    fig, ax = plt.subplots(figsize=(8,8))
    seaborn.heatmap(
      correlation_matrix,
      cmap='seismic',
      vmin=-1.,
      vmax=1.,
      xticklabels=False,
      yticklabels=False,
      ax=ax
    )
    ax.tick_params(left=False, bottom=False)
    
    if filename:
      fig.savefig(
        filename,
        # format = 'pdf', 
        bbox_inches = 'tight'
      )

    if show:
      plt.show()

def energy_plot(steps, energies, filename = None, show = False):
  with plot_style():
    fig, ax = plt.subplots(figsize=(4,3.))
    ax.plot(steps, energies)

    ax.set_xlabel('steps')
    ax.set_ylabel(r'$\mathcal{H}$')
    ax.set_xlim(steps[0], steps[-1])
    
    if filename is not None:
      fig.savefig(
        filename,
        # format = 'pdf',
        bbox_inches = 'tight'
      )

    if show:
      plt.show()
      
def eigenvalues_plot(*args, bins=None, mp = False, show = False, filename = None, **kwargs):
  with plot_style():
    fig, ax = plt.subplots(figsize=(4,3.))

    try:
      ax.set_xlim(kwargs['xlim'])
    except KeyError:
      pass

    try:
      ax.set_ylim(0., kwargs['ylim'])
    except KeyError:
      pass

    try:
      ax.set_yscale(kwargs['yscale'])
    except KeyError:
      pass
    
    for evs, label in args:
      try:
        xlim = kwargs['xlim']
        plot_evs = evs[np.logical_and(xlim[0]<=evs,evs <= xlim[1])]
      except KeyError:
        plot_evs = evs
        
      n, bins, _ = ax.hist(
        plot_evs,
        range=kwargs['xlim'] if 'xlim' in kwargs else None,
        density=True,
        bins=int(2.5*np.sqrt(len(plot_evs))) if bins is None else bins,
        alpha = 0.3 + 0.7/len(args),
        label = label
      )

    if mp:
      # Plot Marcenko-Pastur
      N = kwargs['N']
      T = kwargs['T']
      lambda_m = (1 - np.sqrt(N/T))**2
      lambda_p = (1 + np.sqrt(N/T))**2
      lambda_space = np.linspace(lambda_m, lambda_p, 300)

      rho = T/N * np.sqrt((lambda_p-lambda_space)*(lambda_space-lambda_m)) / (2*np.pi*lambda_space)
      ax.plot(lambda_space, rho, label = f'Marcenko-Pastur $N={N}, T={T}$')

    ax.legend()

    if filename:
      fig.savefig(
        filename,
        # format = 'pdf',
        bbox_inches = 'tight'
      )

    if show:
      plt.show()

## Matrix manipulation
def block_average(matrix, blocks):
  c = len(blocks)

  psum_blocks = [0]
  for b in blocks:
    psum_blocks.append(psum_blocks[-1]+b)

  res = np.zeros((c, c))
  for i in range(c):
    for j in range(c):
      res[i][j] = np.mean(matrix[psum_blocks[i]:psum_blocks[i+1], psum_blocks[j]:psum_blocks[j+1]])
  
  return np.repeat(
    np.repeat(
      res,
      blocks,
      axis=0
    ),
    blocks,
    axis=1
  )

## Information theory
def labels_to_partition(labels):
  partition = {l:[] for l in set(labels)}
  for i,l in enumerate(labels):
    partition[l].append(i)
  return list(map(set, partition.values()))


def shared_information_distance(X,Y):
  assert(len(X) == len(Y))
  n = len(X)

  pX = labels_to_partition(X)
  pY = labels_to_partition(Y)

  p = [len(px)/n for px in pX]
  q = [len(py)/n for py in pY]
  r = {(i,j): len(px & py)/n for i,px in enumerate(pX) for j,py in enumerate(pY)}

  sid = 0.
  for i in range(len(p)):
    for j in range(len(q)):
      if r[(i,j)] > 0.:
        sid -= r[(i,j)] * (np.log(r[(i,j)]/p[i]) + np.log(r[(i,j)]/q[j]))

  return sid
