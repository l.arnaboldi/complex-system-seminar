import numpy as np

class RMTFilter():
  def __init__(self, correlation_matrix):
    self.correlation_matrix = correlation_matrix
    self.N = correlation_matrix.shape[0]

    # Diagonalization algorithm specific for symmetric matrix. 
    # The eigenvalues values are in asciending order
    self.eigval, self.eigvec = np.linalg.eigh(self.correlation_matrix)

  def extract_market_mode(self):
    self.Cm = self.eigval[-1] * np.einsum('i,j->ij',self.eigvec[:,-1],self.eigvec[:,-1])
    # print(self.eigval[-1])
    return self.Cm

  def extract_noise(self, method = 'mp', **kwargs):
    if method == 'mp':
      # Marcenko - Pastur
      self.lambda_plus = (1. + np.sqrt(self.N/kwargs['T']))**2

      noise_eigval = self.eigval <= self.lambda_plus
      self.Cr = np.einsum(
          'l,il,jl->ij',
          self.eigval[noise_eigval],
          self.eigvec[:,noise_eigval],
          self.eigvec[:,noise_eigval]
      )
    elif method == 'random_shuffle':
      # Random shuffle
      raise NotImplementedError()
    else:
      raise NotImplementedError(f'Method "{method}" not known.')
    
    return self.Cr

  def extract_structure(self):
    if not hasattr(self, 'Cr'):
      raise ValueError('The noise has not been filtered!')

    Cm = np.zeros_like(self.correlation_matrix)
    if hasattr(self, 'Cm'):
      Cm = self.Cm

    return self.correlation_matrix - self.Cr - Cm


