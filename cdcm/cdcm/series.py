import numpy as np

class TimeSeries():
  def __init__(self, raw_data):
    assert(len(raw_data.shape) == 2)
    self.raw_data = raw_data
    self.N, self.T = raw_data.shape

  def standardize(self):
    average = np.mean(self.raw_data, axis=1, keepdims=True)
    std = np.std(self.raw_data, axis=1, keepdims=True)

    self.standardized = (self.raw_data-average)/std
    return self.standardized

  def compute_correlation(self):
    if not hasattr(self, 'standardized'):
      self.standardize()
    self.correlation = np.einsum(
      'it,jt->ij',
      self.standardized,
      self.standardized
    )/self.T

    assert(self.correlation.shape == (self.N, self.N))
    return self.correlation

  def shuffled(self, seed = None):
    rng = np.random.RandomState(seed)
    shuffled_raw_data = np.copy(self.raw_data)

    for ts in shuffled_raw_data:
      rng.shuffle(ts)
    # rng.shuffle(shuffled_raw_data)
    return TimeSeries(shuffled_raw_data)


