from fileinput import filename
import numpy as np
import networkx as nx
from tqdm import tqdm

## Supporting Data structures
class Edge():
  def __init__(self, v, u):
    self.v = v
    self.u = u
  
class WeightedEdge(Edge):
  def __init__(self, v, u, w):
    super().__init__(v,u)
    self.w = w

  def __ge__(self, other):
    try:
      return self.w >= other.w
    except AttributeError:
      return self.w >= other
  
  def __lt__(self, other):
    return self.w < other.w


class UnionFind():
  def __init__(self, n):
    self.n_elements = n
    self.n_sets = n
    self.parent = [i for i in range(n)]
    self._rank = [0 for _ in range(n)]
    self._size = [1 for _ in range(n)]

  def set(self, a):
    if self.parent[a] != a:
      self.parent[a] = self.set(self.parent[a])

    return self.parent[a]

  def join(self, a ,b):
    pa = self.set(a)
    pb = self.set(b)

    if pa != pb:
      if self._rank[pb] > self._rank[pa]:
        pa, pb = pb, pa # Swap the two. Guarantee that pa ha higher rank
      
      self.parent[pb] = pa
      self._size[pa] += self._size[pb]
      self._size[pb] = 0
      self.n_sets -=1

      if self._rank[pa] == self._rank[pb]:
        self._rank[pa] += 1
  
  def same_set(self, a, b):
    return self.set(a) == self.set(b)

  def set_size(self, a):
    return self._size[self.set(a)]

## Method implementation
class BaseGraphMethod():
  def __init__(self, C):
    self.n = C.shape[0]
    self.adjiacency_matrix = C

    # Build Edge List
    self.edge_list = []
    for i in range(self.n):
      for j in range(i+1, self.n):
        self.edge_list.append(WeightedEdge(i,j,C[i][j]))
    
class AssestGraph(BaseGraphMethod):
  def __init__(self, C, threshold = None):
    super().__init__(C)

    # if threshold is None:
    #   self.threshold = self.estimate_threshold()
    # else:
    self.threshold = threshold

  def estimate_threshold(self, T, tau = 2.):
    self.threshold = (np.exp(2*tau/np.sqrt(T-3))-1)/(np.exp(2*tau/np.sqrt(T-3))+1)               

  def fit(self):
    self.filtered_edge_list = [
      Edge(e.v, e.u) for e in self.edge_list if e >= self.threshold
    ]

  def find_communities(self):
    if not hasattr(self, 'filtered_edge_list'):
      return ValueError('You did not fit the model!')

    uf = UnionFind(self.n)

    for e in self.filtered_edge_list:
      uf.join(e.v, e.u)

    parent_to_community = {p:c for c, p in enumerate(set(uf.parent))}

    return np.array(
      list(map(lambda p: parent_to_community[p], uf.parent)),
      dtype=np.int16
    )

  
class MinimumSpanningTree(BaseGraphMethod):
  def fit(self):
    uf = UnionFind(self.n)

    sorted_edge_list = sorted(self.edge_list, reverse=True)
    edge_index = 0
    self.filtered_edge_list = []
    while uf.n_sets > 1:
      e = sorted_edge_list[edge_index]
      edge_index += 1

      if uf.same_set(e.v, e.u):
        continue
      
      self.filtered_edge_list.append(e)
      uf.join(e.v, e.u)
    
    assert(len(self.filtered_edge_list) == self.n-1)

  def find_communities(self, n_communities):
    if not hasattr(self, 'filtered_edge_list'):
      return ValueError('You did not fit the model!')

    uf = UnionFind(self.n)

    for e in self.filtered_edge_list:
      if uf.n_sets == n_communities:
        break
      uf.join(e.v, e.u)

    parent_to_community = {p:c for c, p in enumerate(set(uf.parent))}

    return list(
      map(
        lambda p: parent_to_community[p],
        map(
          lambda v: uf.set(v),
          range(uf.n_elements)
        )
      )
    )

class PMFG(MinimumSpanningTree):
  def fit(self, status = True):
    nx_graph = nx.Graph()

    sorted_edge_list = sorted(self.edge_list, reverse=True)
    edge_index = 0
    self.filtered_edge_list = []
    with tqdm(total = 3*(self.n - 2), disable=(not status)) as pbar:
      while nx_graph.number_of_edges() < 3*(self.n - 2):
        e = sorted_edge_list[edge_index]
        edge_index += 1

        nx_graph.add_edge(e.u, e.v)
        if not nx.is_planar(nx_graph):
          nx_graph.remove_edge(e.u, e.v)
        else:
          self.filtered_edge_list.append(e)
          pbar.update()

  ## The Hierarchical Organization Thm in Tumminello et al. prove that find_communities
  #  method is the same both for PMFG and MST.
      
## Plotting
import matplotlib.pyplot as plt
from .utilities import plot_style

def graph_plot(n, edge_list, pos = None, labels = None, communities = None, filename = None):
  g = nx.Graph()

  for v in range(n):
    g.add_node(v)

  for e in edge_list:
    g.add_edge(e.u, e.v)

  if pos is None:
    from networkx.drawing.nx_pydot import graphviz_layout
    # pos = nx.planar_layout(g)
    pos = graphviz_layout(g, prog='dot')

  if communities is not None:
    clabels = set(communities)
    palette = plt.get_cmap('gist_rainbow')
    label_to_color = {l:palette(i/(len(clabels)-1)) for i,l in enumerate(clabels)}
    color_map = list(map(lambda l: label_to_color[l], communities))
  else:
    color_map = None

  with plot_style():
    fig, ax = plt.subplots(figsize=(8,8))
    nx.draw(
      g,
      pos = pos,
      ax=ax,
      node_size = 100.,
      node_color = color_map,
      labels = ({v:l for i,l in enumerate(labels)} if labels is not None else None)
    )
    
    if filename is not None:
      fig.savefig(filename, bbox_inches = 'tight')
  
  return pos